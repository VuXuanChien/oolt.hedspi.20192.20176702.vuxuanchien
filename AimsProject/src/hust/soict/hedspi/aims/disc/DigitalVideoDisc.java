package hust.soict.hedspi.aims.disc;

import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.CompactDisc;
import hust.soict.hedspi.aims.media.Disc;
import hust.soict.hedspi.aims.media.Media;

public class DigitalVideoDisc extends Disc{
	
	public DigitalVideoDisc() {
		
	}

	public DigitalVideoDisc(String title, String category, String director, float cost, int length) {
		super(title, category, director, cost, length);
		// TODO Auto-generated constructor stub
	}
	
	public void play() {
		System.out.println("Playing DVD: " + super.getTitle());
		System.out.println("DVD length: " + super.getLength());
	}
	
//	public int compareTo(Media media) {
//		if(media instanceof DigitaVideoDisc) {
//			
//		}
//		return (super.getCost()>((Media)o).getCost()) ?1:-1;
//	}
	
	@Override
	public int compareTo(Media media) {
		if(media instanceof DigitalVideoDisc) {
		   return (this.getLength()>((DigitalVideoDisc)media).getLength()) ?1:-1;
		}else {
			return super.compareTo(media);
		}
	}
	
}
