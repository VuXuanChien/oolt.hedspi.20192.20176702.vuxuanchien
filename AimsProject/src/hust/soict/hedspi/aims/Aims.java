package hust.soict.hedspi.aims;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import hust.soict.hedspi.aims.disc.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.Media;
import hust.soict.hedspi.aims.order.Order;
import hust.soict.hedspi.aims.media.CompactDisc;
//import hust.soict.hedspi.aims.MemoryDaemon;

public class Aims {
	public static void main(String[] args) {
		
		List<Media> allProducts =new ArrayList<Media>();
		Order order= new Order();
		Scanner input = new Scanner(System.in);
		
		//Initialize data..
		DigitalVideoDisc dvd1= new DigitalVideoDisc("Bhe LionKing","film","Tran Quang",1233,543);
		DigitalVideoDisc dvd2= new DigitalVideoDisc("Tiger King","film","Tran Quang",132,345);
		DigitalVideoDisc dvd3= new DigitalVideoDisc("Snake king","film","Tran Quang",15,204);
		DigitalVideoDisc dvd4= new DigitalVideoDisc("Monkey King","film","Tran Quang",143,2340);
		CompactDisc cp2 =new CompactDisc("DangerousWoman","music",20,"ariana");
		CompactDisc cp1 =new CompactDisc("Kamikaze","music",200,"Eminem");
		CompactDisc cp3 =new CompactDisc("Not Afraid","music",100,"Dreezy");
		CompactDisc cp4 =new CompactDisc("Indigo","music",69,"Breezy");
		Book book1= new Book("Rung xa nu","book",10);
		Book book2=new Book("Tam cam","book",2);
		Book book3=new Book("Vo nhat","book",40);
		Book book4=new Book("Coding","book",201);
		book1.addAuthor("Tran Quang");
		book1.addAuthor("Tran Minh");
		book1.addAuthor("Tran An");
		book2.addAuthor("ng");
		book3.addAuthor("Tr uang");
		book4.addAuthor("Trang");
		book4.addAuthor("Tuang");
		book3.addAuthor("an Quang");
		cp1.addTrackToCompactDisc("Every day", 3);
		cp1.addTrackToCompactDisc("Ev y", 5);
		cp2.addTrackToCompactDisc("sadf day", 32);
		cp3.addTrackToCompactDisc("saf day", 334);
		cp4.addTrackToCompactDisc("saf day", 676);
		cp4.addTrackToCompactDisc("Evefsary day", 36534);
		cp3.addTrackToCompactDisc("Everysf day", 768);
		cp3.addTrackToCompactDisc("as day", 35);
		cp2.addTrackToCompactDisc("Eversay day", 38);
		allProducts.add(book1);
		allProducts.add(book2);
		allProducts.add(book3);
		allProducts.add(book4);
		allProducts.add(dvd1);
		allProducts.add(dvd2);
		allProducts.add(cp1);
		allProducts.add(dvd3);
		allProducts.add(cp2);
		allProducts.add(dvd4);
		allProducts.add(cp3);
		allProducts.add(cp4);
		
		//sort
		Collections.sort(allProducts);
		boolean cont = true;
		do {
			System.out.println("===========================");
			System.out.println("Order Management Application");
			System.out.println("--------Buyer-----------");
			System.out.println("1.Display All product");
			System.out.println("2. Create new order");
			System.out.println("3.Add item to the order");
			System.out.println("4.Delete item by ID");
			System.out.println("5.Display items ordered");
			System.out.println("--------Admin-----------");
			System.out.println("6.Create Book");
			System.out.println("7.Create CompactDisc");
			System.out.println("8.Create DigitalVideoDisc");
			System.out.println("0.Exit");
			System.out.println("-------------------");
			System.out.println("Please choose a number: 0-1-2-3-4-5-6-7-8");	
			int chon = input.nextInt();
			switch (chon) {
			case 1:
				menuNormal(allProducts);
				//menuDetails(allProducts);
				break;
			case 2:
				Order order1= new Order();
				order=order1;
				System.out.println("Order created");
				break;
			case 3:
				  System.out.println("Nhap Id mat hang can chon");
				  int myint1 = input.nextInt();
				  if(myint1>(allProducts.size()-1)) {
					  System.out.println("Sai Id");
					  System.out.println("---------------------");
				  }else {
					  System.out.println("Nhap so luong");
					  int myint2 = input.nextInt();;
					  allProducts.get(myint1).setQtyOrder(myint2); 
					  order.addOrderProduct(allProducts.get(myint1));
					  System.out.println("THEM THANH CONG VAO HOA DON");
					  System.out.println("---------------------");
				  }
				break;
			case 4:
				  System.out.println("NHAP ID MAT HANG CAN XOA!!");
				  int myint3 = input.nextInt();
				  if(myint3>(allProducts.size()-1)) {
					  System.out.println("SAI ID,HAY KIEM TRA LAI");
					  System.out.println("---------------------");
				  }else {
					  System.out.println("NHAP SO LUONG MUON XOA");
					  int myint4 = input.nextInt();
					  order.removeProduct(allProducts.get(myint3), myint4); 
						  System.out.println("---------------------"); 
				  }
				break;
			case 5:
				order.inHoaDon();
				break;
			case 6:
				Book book =new Book();
				System.out.println("Author:");
				String author =input.nextLine();
				input.nextLine();
				System.out.println("Category:");
				String category =input.nextLine();
				System.out.println("BookName:");
				String bookname =input.nextLine();
				System.out.println("Price:");
				float price = input.nextFloat();
				input.nextLine();
				book= createBook(bookname,category,price,author);
				if(allProducts.contains(book)){
					System.out.println("Quyen sach da ton tai trong kho");
				}else {
				    allProducts.add(book);
				    System.out.println("Them thanh cong "+book.getTitle());
				}
				break;
			case 7:
				System.out.println("CompactDisc Title:");
				String discName =input.nextLine();
				String discName1 =input.nextLine();
				System.out.println("Category:");
				String discCategory =input.nextLine();
				System.out.println("Price:");
				float discPrice =input.nextFloat();
				System.out.println("Artist:");
				String discArtist = input.nextLine();
				input.nextLine();
				CompactDisc disc =new CompactDisc(discName1,discCategory,discPrice,discArtist);
				if(allProducts.contains(disc)) {
					System.out.println("Da to tai Album trong kho!!!");
					break;
				}
				int choose=1;
				while(choose==1) {
				System.out.println("Add Track?(1 to continue)");
				choose= input.nextInt();
				if(choose==1) {
				System.out.println("Track Name:");
				String trackName= input.nextLine();
				String trackName1= input.nextLine();
				System.out.println("Length:");
				int trackLength= input.nextInt();
				disc.addTrackToCompactDisc(trackName1, trackLength);
				}
				}
				int choose1=1;
				while(choose1==1) {
				System.out.println("Play the Album?(1 to play)");
				choose1= input.nextInt();
				if(choose1==1) {
				  disc.playList();
				}
				}
				allProducts.add(disc);
				break;
			case 8:
				System.out.println("Digitalvideodisc Title:");
				String dgtName =input.nextLine();
				String dgtName1 =input.nextLine();
				System.out.println("Category:");
				String dgtCategory =input.nextLine();
				System.out.println("Director:");
				String dgtDirector =input.nextLine();
				System.out.println("Price:");
				float dgtPrice =input.nextFloat();
				System.out.println("Length:");
				int dgtLength =input.nextInt();
				DigitalVideoDisc dgt =new DigitalVideoDisc(dgtName1,dgtCategory,dgtDirector,dgtPrice,dgtLength);
				if(allProducts.contains(dgt)) {
					System.out.println("Da to tai Disc trong kho!!!");
					break;
				}
				int choose2=1;
				while(choose2==1) {
				System.out.println("Play the Disc?(1 to play)");
				choose2= input.nextInt();
				if(choose2==1) {
				  dgt.play();
				}
				}
				allProducts.add(dgt);
				break;
			default:
				System.out.println("Appclosed");
				cont = false;
				break;
			}
		} while (cont);
	} //end main
	
	public static void menuDetails(List<Media> allProducts) {
		System.out.println("All products:");
		System.out.println("==================================");
		//System.out.printf("%-3s%-15s%-10s%-3s\n","ID","Title","Category","Price");
		int id=0;
		if(allProducts.size()== 0 ) {
			System.out.println("Cua hang chua co san pham nao!!!!");
		}
		for(Media media:allProducts ) {
			//System.out.printf("%-3d%-15s%-10s%-3.2f\n",id,media.getTitle(),media.getCategory(),media.getCost());
			media.showInfo();
			System.out.println("===================================");
			id++;
		}
}

    public static void menuNormal(java.util.Collection<Media> allProducts) {
		System.out.println("All products:");
		System.out.println("==================================");
		System.out.printf("%-3s%-15s%-10s%-3s\n","ID","Title","Category","Price");
		int id=0;
		if(allProducts.size()== 0 ) {
			System.out.println("Cua hang chua co san pham nao!!!!");
		}
		for(Media media:allProducts ) {
			System.out.printf("%-3d%-15s%-10s%-3.2f\n",id,media.getTitle(),media.getCategory(),media.getCost());
			id++;
		}
}
	
	static public Book createBook(String name,String category,float price,String author) {
		Book book =new Book(name,category,price);
		book.addAuthor(author);
		return book;
	}
	
} //end Class
