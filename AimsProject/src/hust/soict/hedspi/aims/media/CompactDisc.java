package hust.soict.hedspi.aims.media;

import java.util.ArrayList;
import java.util.List;
import hust.soict.hedspi.aims.media.Track;

public class CompactDisc extends Disc{
	private String artist;
	private int totalLength=0;// tong do dai ca Album
	private List<Track> tracks =new ArrayList<Track>();
	
	public CompactDisc() {
		
	}

	public CompactDisc(String title, String category,float cost,String artist) {
		super(title, category,cost);
		this.artist=artist;
		// TODO Auto-generated constructor stub
	}
	//them track vao dia
	public boolean addTrackToCompactDisc(String title,int length) {
		Track track= new Track(title,length);
		if(tracks.contains(track)) {
			System.out.println(track.getTitle()+"da ton tai trong playlist nay");
			return false;
		}
		if(tracks.add(track)) {
			this.totalLength+=length;
			return true;
		}
		return false;
	}
	//xoa track khoi dia bang ten 
	public void removeTrackFromCompact(String title) { 
		for(int i=0;i<this.tracks.size();i++) {
			if(this.tracks.get(i).getTitle()==title) {
				this.totalLength-=this.tracks.get(i).getLength();
				tracks.remove(i);
				return;
			}
		}
		System.out.println("Playlist khong co track "+title);
		return;
	}
	
	public void playList() {
		if(this.tracks.size()==0){
			System.out.println("Playist chua co bai hat nao!!!");
			System.out.println("=====================================");
			return;
		}
		for(Track track:this.tracks) {
			track.play();
			System.out.println("=====================================");
		}
	}
	
	public void showInfo() {
		System.out.println("CD Title: "+super.getTitle());
		System.out.println("CD Category: "+super.getCategory());
		System.out.println("CD Artist: "+this.getArtist());
		System.out.println("CD Cost: "+super.getCost());
		System.out.println("---------------------------------------");
		if(this.totalLength==0) {
			System.out.println("This CompactDisc dont have any song");
		}else {
		for(Track track:this.tracks) {
			System.out.println("Track Title: "+track.getTitle());
			System.out.println("Track length : "+track.getLength());
		}
		System.out.println("=====================================");
		System.out.println("Total length : "+this.getTotalLength());
		}
	}
	

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}
	
	public int getTotalLength() {
		return totalLength;
	}
	public void setTotalLength(int totalLength) {
		this.totalLength = totalLength;
	}
	
//	public int compareTo(Object o) {
//		return (super.getCost()>((Media)o).getCost()) ?1:-1;
//	}
	@Override
	public int compareTo(Media media) {
		if(media instanceof CompactDisc) {
		   return (this.getTotalLength()>((CompactDisc)media).getTotalLength()) ?1:-1;
		}else {
			return super.compareTo(media);
		}
	}
}
