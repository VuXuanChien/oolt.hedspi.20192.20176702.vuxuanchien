package hust.soict.hedspi.aims.media;

public abstract class Media implements Comparable<Media> {
	private String title;
	private String category;
	private float cost;
	private int qtyOrder; //so luong san pham
	//contructor
	public Media(){
		this.title="This is title";
		this.category="this is category";
		this.cost=0;
	}
	public Media(String title,String category,float cost) {
		this.title=title;
		this.category=category;
		this.cost=cost;
	}
	//getter and Setter Methods
	public int getQtyOrder() {
		return qtyOrder;
	}
	public void setQtyOrder(int qtyOrder) {
		this.qtyOrder = qtyOrder;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	//show title,category
	public void showInfo() {
		System.out.println("Title:"+this.title);
		System.out.println("Category:"+this.category);
		System.out.println("Cost:"+this.cost);
	}
	
	//overeide equal method
	public boolean equals(Object obt)
	{
		if(this.title.equalsIgnoreCase(((Media)obt).title)) {
			return true;
		}
		return false;
	}
	
	public int compareTo(Media media) {
//		if( this.category.compareTo(media.category)==0) {
//			//return ( ((Disc)this).getLength() > ( (Disc)media ).getLength() ) ? 1 : -1;
//			return (this.cost>media.cost) ?1:-1;
//		}
		return this.getCategory().compareTo(media.getCategory());
	}
	
}
