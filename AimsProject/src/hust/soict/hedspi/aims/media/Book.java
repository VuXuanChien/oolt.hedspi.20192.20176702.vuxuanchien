package hust.soict.hedspi.aims.media;

import java.util.ArrayList;
import java.util.List;

public class Book extends Media{
	//title,cost,category
	private List<String> authors =new ArrayList<String>();
	
	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	public int getNumberAuthor() {
		return authors.size();
	}
	
	public Book() {
		
	}

	public Book(String title,String category,float cost) {
		super.setTitle(title);
		super.setCategory(category);
		super.setCost(cost);
	}
	
	//them tac gia bao book
	public boolean addAuthor(String name) {
		if(this.authors.indexOf(name)!=-1) {
			return false;
		}else {
			this.authors.add(name);
			return true;
		}
	}
	//xoa tac gia khoi book
	public boolean removeAuthor(String name) {
		if(this.authors.indexOf(name)==-1) {
			return false;
		}else {
			int index=this.authors.indexOf(name);
			this.authors.remove(index);
			return true;
		}
	}
	//in thong tin sach
	public void showInfo() {
		super.showInfo();
		for(String author:this.authors) {
			System.out.println("Author:"+author);
		}
	}

	
	public int compareTo(Media media) {
		if(media instanceof Book) {
		   //return (this.authors.size()>((Book)media).authors.size()) ?1:-1;
		   return (super.getCost()>media.getCost())?1:-1;
		}else {
			return super.compareTo(media);
		}
	}
		
    }
	