package hust.soict.hedspi.aims.media;

import java.util.Scanner;

public class Date {
	private int day;
	private String month;
	private int year;
	
	//constructer
	
		public Date() {
			this.day=01;
			this.month="January";
			this.year=2000;
		}
		
		public Date(int day,String month,int year) {
			this.day=day;
			this.month=month;
			this.year=year;
		}
		
		public Date(String dateTime) {
			String day1=null;
			String month1=null;
			String year1=null;
			int count=0;
			int endMonth=0;
			int endDay=0;
			for(int i=0;i<dateTime.length();i++)
			{
				if(dateTime.charAt(i)==' ') {
					if(count ==0) {
						month1=dateTime.substring(0,i);
						endMonth=i+1;
					}
					if(count==1) {
						day1=dateTime.substring(endMonth, i);
						endDay=i+1;
					}
					if(count==2) {
						year1=dateTime.substring(endDay, i);
					}
					count++;
				}
			}
			this.month=month1;
			this.day=Integer.parseInt(day1);
			this.year=Integer.parseInt(year1);
		}
		
		//setters getters method

		public int getDay() {
			return day;
		}

		public void setDay(int day) {
			this.day = day;
		}

		public String getMonth() {
			return month;
		}

		public void setMonth(String month) {
			this.month = month;
		}

		public int getYear() {
			return year;
		}

		public void setYear(int year) {
			this.year = year;
		}
		
		
		//user Input
		
		public void MyDate(String dateTime) {
			String day1=null;
			String month1=null;
			String year1=null;
			int count=0;
			int endMonth=0;
			int endDay=0;
			for(int i=0;i<dateTime.length();i++)
			{
				if(dateTime.charAt(i)==' ') {
					if(count ==0) {
						month1=dateTime.substring(0,i);
						endMonth=i+1;
					}
					if(count==1) {
						day1=dateTime.substring(endMonth, i);
						endDay=i+1;
					}
					if(count==2) {
						year1=dateTime.substring(endDay, i);
					}
					count++;
				}
			}
			this.month=month1;
			this.day=Integer.parseInt(day1);
			this.year=Integer.parseInt(year1);
		}
		
		public void accept() {
			String dateTime;
			Scanner scanner=new Scanner(System.in);
			System.out.println("nhap chuoi ngay thang nam co dang \"Month Day Year\" :");
			dateTime=scanner.nextLine();
			this.MyDate(dateTime);
		}
		
		//print
		public void printDateTime() {
			System.out.println(this.getDay()+" "+this.getMonth()+" "+this.getYear());
		}
		

}
