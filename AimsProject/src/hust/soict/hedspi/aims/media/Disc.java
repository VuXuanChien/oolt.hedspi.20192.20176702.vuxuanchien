package hust.soict.hedspi.aims.media;

public class Disc extends Media {
	//title,category,cost//qtyOrder
	private String director;
	private int length;
	
	//contructor
	public Disc() {
	}
	
	public Disc(String title,String category,float cost) {
		super.setTitle(title);
		super.setCategory(category);
		super.setCost(cost);
	}
	
	public Disc(String title,String category,String director,float cost,int length)
	{
		super.setTitle(title);
		super.setCategory(category);
		super.setCost(cost);
		this.director=director;
		this.length=length;
	}
	
	//getter and Setter Methods
	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
	
	//show title,category,cost,directior,length
	public void showInfo() {
		super.showInfo();
		System.out.println("Director: "+this.director);
		System.out.println("Length: "+ this.length);
	}

//	@Override
//	public int compareTo(Media o) {
//		return 0;
//	}

}
