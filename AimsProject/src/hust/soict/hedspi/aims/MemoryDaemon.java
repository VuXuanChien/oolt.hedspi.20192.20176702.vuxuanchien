package hust.soict.hedspi.aims;
import java.lang.Runnable;

public class MemoryDaemon {
	
	private long memoryUsed =0;

	public MemoryDaemon() {
		// TODO Auto-generated constructor stub
	}
	
	public void run() {
		Runtime rt =Runtime.getRuntime();
		long used;
		while(true) {
			used=rt.totalMemory()-rt.freeMemory();
			if(used != this.memoryUsed ) {
				System.out.println("Memory used= "+used);
				this.memoryUsed=used;
			}
		}
	}

}
