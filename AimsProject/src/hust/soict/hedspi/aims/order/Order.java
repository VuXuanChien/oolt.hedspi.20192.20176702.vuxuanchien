package hust.soict.hedspi.aims.order;

import hust.soict.hedspi.aims.media.Media;

public class Order{
	private int totalCost=0;
	private Media orderProduct[] = new Media[20]; //Mang chua cac doi tuong Media
	private int qtyOrderProduct=0;    //so luong cac loai san pham trong hoa don
	
	//them san pham vao hoa don
	public void addOrderProduct(Media media){
		this.totalCost+=media.getCost()*media.getQtyOrder();
		this.orderProduct[qtyOrderProduct]=media;
		this.qtyOrderProduct++;	
	}
	
	public void removeProduct(Media media,int qty) {
		if(qty>media.getQtyOrder() ) {
			System.out.println("So luong muon xoa nhieu hon hien tai");
			return;
		}
		int conLai = media.getQtyOrder()-qty;
		this.totalCost-=media.getCost()*qty;
		media.setQtyOrder(conLai);
		System.out.println("Xoa Thanh Cong");
	}
	
	//getter and setter method
	public int getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(int totalCost) {
		this.totalCost = totalCost;
	}
	
	//In san pham trong hoa don
	public void inHoaDon()
	{
		System.out.println("*******HOA DON BAN HANG*******");
		System.out.printf("%-15s%-10s%-10s%-10s\n","ProductName","Price","Quantity","Total");
		for(int i=0;i<this.qtyOrderProduct;i++) {
			System.out.printf("%-15s%-10.2f%-10d%-10.2f\n",orderProduct[i].getTitle(),orderProduct[i].getCost(),orderProduct[i].getQtyOrder(),orderProduct[i].getCost()*orderProduct[i].getQtyOrder());			
		}
		System.out.println("*******Total*******");
		System.out.println("TONG SO TIEN PHAI TRA:"+this.getTotalCost());
		
	}
	
}
